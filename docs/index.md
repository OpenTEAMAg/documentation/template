# About

![Hands image from ](image.png)
*A header image is a nice idea - [Image source](https://bibliotheca-laureshamensis-digital.de/bav/bav_pal_lat_1449)*


## OpenTEAM documentation

We make these static sites as a persistent record of work that we want to be available for others to share as a reference. This includes outcomes from Collabathons, working groups, or specific time-based projects that we intend to maintain over time. Making & hosting these resources in Gitlab is a great way to make sure that all parts of our process are as shareable and replicable as possible. 

[You can see our community documentation pages here.](https://openteam.community/community-documentation/) 

We believe in documenting not just project outputs, but also process and context. 

* If we can make more resources available, it makes the lift for each individual lighter
* If we can lower the barriers to entry, it broadens the field for new participants to contribute 
* If we can make collaboration easier, we can distribute the resources we have more effectively across multiple people or groups 

To that end, please copy, share, and make improvements to this template! 

**Support:**


* Contact vic@openteam.community
* Post on the [GOAT Forum](https://forum.goatech.org/)
* Find us on [Hylo](https://www.hylo.com/groups/openteam)




## This Page

Index.md is the index or landing page for your site, & the one that the default URL for your project will lead to - make sure that as you make new pages and reorganize the template site for your project needs, you keep this one! 

Once you've forked your repo, you can delete all the information on this page and replace it with whatever is relevant to your project. Don't worry, you can always see the original version [here](https://openteamag.gitlab.io/documentation/template/).  

----

*Here are some examples of things to have in your index page:* 

# Project Overview

*authoring group and date*

Description of what the thing is about, when it happened, and what the top-level outcomes were

Links to find out more about the broad context of your work are also good. 

**About**

[OpenTEAM - How We Work](https://openteam.community/how-we-work/)

## Problem Statement

Here's the thing we're working on, and all the questions we are trying to answer with this project. 

## Results Statement

Here's a statement summarizing all the work we did after the project concluded. 