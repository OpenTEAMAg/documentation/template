# Documentation

## Strategies for building shareable community resources
 
At OpenTEAM, we're advocates for documentation at every part of a project. Working in collaborative spaces, there are so many valuable things that could get lost or forgotten if we don't make a concerted effort to capture them. At the foundation, The question we’re trying to answer with documentation resources is: **What would it take for someone else to come behind you and pick up where you left off?** 

Much of the documentation we see in the open source community focuses on  what lines of code do, how to use and expand upon a library, or other ways of describing the *outcome* of a project. There is a wealth of knowledge and value in being able to use the output of another person or group’s hard work in this way. We also see value in not only sharing the final output, but documenting the *context* and *process* in which it was created. This process of codesign is in of itself worth documenting, as it can inform how our community works together on future projects and help other groups learn from our successes and frustrations. 

### Why?

In our community, we have a wide range of people, from farmers to software developers, with different values and motivations. One thing we’ve clearly learned from working together is that we can’t assume we have a shared set of assumptions about what’s important, or what’s obvious, or what’s common knowledge.

We believe in [FAIR principles,](https://www.nature.com/articles/sdata201618) not just for research data, but for everything we’re trying to do together. The goal is that we can support replicating everything shareable in useful, comprehensible ways that allow for evolution & mutual exchange beyond our initial design.


### What does this look like? 

* Acting in ways that invite contribution 
* Insisting on the importance of working together, & affirming value of connected networks 
* Investing the time and resources for documenting your work in a way that is legible to people who aren’t already familiar with it 
* Developing our fluency in communication as a fundamental skill for openness
* Partnering with people who are good at translating across conceptual fields
* Creating machine-readable and human readable documentation


## In Brief

**About page** - Do you have a single place where you tell about your project, and that you can share with other people? 
> Does that page answer who, what, when, where, & why the project exists for? 

**Project Context**- When describing your project, what assumptions do you make about previous knowledge? Can you find a way of saying what those assumptions are in your documentation?
> E.g. “this project assumes familiarity with x. For more information about x, start [here](https://www.sociocracyforall.org/sociocracy/).” 

**Invite Engagement** - If someone new finds your project and has a question about it, is there a place for them to ask?

> If you’re working on a generalizable workflow, you can do a search with your terms and pray that somebody has asked & answered it already on StackOverflow. But the more unique & specific the question is, the less likely someone is to have answered it before– and the more valuable it will be to capture it and get it answered. 

> FarmOS uses community calls & forums for this – and we also have GOAT forums & Hylo. Even something like sharing a contact email & pointing people who have questions toward it can make a difference. 

**Ask** - What do you want or need help with? 
> We sometimes do this across our community in the form of ‘resource mapping’. But you can also do it for your project- saying the thing you need out loud gives someone the chance to hear you & respond. 


## Resources

- [Grand Unified Theory of Documentation](https://docs.divio.com/documentation-system/) (Divio, via mike stenta via farmOS forum)
- [BeautifulDocs](https://github.com/matheusfelipeog/beautiful-docs)
- [WriteTheDocs community - Beginner’s Guide](https://www.writethedocs.org/guide/writing/beginners-guide-to-docs/)

**Community Examples:**

-  [Regen Registry Handbook](https://registry.regen.network/v/regen-registry-handbook)
- [farmOS user guide](https://farmos.org/guide/)
- [In-flight safety instructions](https://i.sstatic.net/O7mFt.jpg) :D 