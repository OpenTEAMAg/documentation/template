# Frequently Asked Questions


## Setting up a Static Site - Gitlab

*Instructions for getting started* 

### Examples

You can find all our community documentation pages here: 

* [OpenTEAM - Community Documentation](https://openteam.community/community-documentation/)

### What do we use these for? 

We make these static sites as a persistent record of work that we want to be available for others to share as a reference. This includes outcomes from Collabathons, working groups, or specific time-based projects that we intend to maintain over time. Making & hosting these resources in Gitlab is a great way to make sure that all parts of our process are as shareable and replicable as possible. 

### How do I make one? 

[Make a Gitlab account](https://gitlab.com/users/sign_in), and request to be added to the O[penTEAM Gitlab](https://gitlab.com/OpenTEAMAg). 

Within the OpenTEAM Gitlab page, there’s a project called [Documentation](https://gitlab.com/OpenTEAMAg/documentation). In it is a sub-project called [OpenTEAM Documentation Template](https://gitlab.com/OpenTEAMAg/documentation/template). This is a project with all the basic categories and structure we use for documentation, but without any specifics included. It’s here to help you get started as you build your own documentation page. 

![Documentation Template Screenshot 1](https://gitlab.com/OpenTEAMAg/documentation/template/-/raw/270e38419d4c5d2bb4ce7d6fa8dcc46a177a58fc/docs/assets/docpage.png)

Select “Fork” from the options in the right-hand corner. From [this page](https://gitlab.com/OpenTEAMAg/documentation/template/-/forks/new), you can name the project you’re about to create, select the URL, and decide if it will be public or private.

![Documentation Template Screenshot 1](https://gitlab.com/OpenTEAMAg/documentation/template/-/raw/270e38419d4c5d2bb4ce7d6fa8dcc46a177a58fc/docs/assets/docpage2.png)

Then fork the project! You can find your new project at the url you specified above, or by navigating through the gitlab menu. 

#### Setting up your project’s address

From the sidebar, select ‘Deploy’ and navigate to ‘Pages’

![Documentation Template Screenshot 1](https://gitlab.com/OpenTEAMAg/documentation/template/-/raw/270e38419d4c5d2bb4ce7d6fa8dcc46a177a58fc/docs/assets/docpage3.png)

On this page, un-select the box that says “Use unique domain”. Gitlab defaults to this option, but it will create error messages & make things tricky with the way we use pages unless you disable this option. 


![Documentation Template Screenshot 1](https://gitlab.com/OpenTEAMAg/documentation/template/-/raw/270e38419d4c5d2bb4ce7d6fa8dcc46a177a58fc/docs/assets/docpage4.png)

Now you’re ready to start adding content to your site. 

The [ReadMe file in both the template](https://gitlab.com/OpenTEAMAg/documentation/template/-/blob/master/README.md?ref_type=heads) and your newly forked project contains detailed instructions for getting set up with the tools you’ll need to create pages - follow these instructions to get started with [GitKraken](https://www.gitkraken.com/) & [VSCode](https://code.visualstudio.com/) so you can begin contributing locally to your project. 

![Documentation Template Screenshot 1](https://gitlab.com/OpenTEAMAg/documentation/template/-/raw/270e38419d4c5d2bb4ce7d6fa8dcc46a177a58fc/docs/assets/docpage5.png)

Clone your repo using GitKraken, then open the project files that you saved to your hard drive. 

The first thing you’ll want to do is change the site URL in both the mkdocs.yml file and the ReadMe file so that they match the new URL for your project that you just made, rather than having the information from the project you cloned. 

![Documentation Template Screenshot 1](https://gitlab.com/OpenTEAMAg/documentation/template/-/raw/270e38419d4c5d2bb4ce7d6fa8dcc46a177a58fc/docs/assets/docpage6.png)

Open the mkdocs.yml file and change the site_url to match your new location -  something like https://openteamag.gitlab.io/codesign/new-project. 

Then do the same thing for the README file.

![Documentation Template Screenshot 1](https://gitlab.com/OpenTEAMAg/documentation/template/-/raw/270e38419d4c5d2bb4ce7d6fa8dcc46a177a58fc/docs/assets/docpage7.png)

Push these changes to Gitlab using GitKraken (this is also described in the README file). 
Now you're ready to add content to the pages in your static site!

Pages are written in Markdown, and this [cheat sheet from MarkdownGuide.org](https://www.markdownguide.org/cheat-sheet/) is a useful resource. If you have any other questions, or have recommendations for making this guide better, please reach out to vic@openteam.community. Thanks for building the archive with us! 